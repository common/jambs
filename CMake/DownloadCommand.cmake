

if(NOT EXISTS "${DOWNLOAD_PATH}")
  execute_process( COMMAND ${CMAKE_COMMAND} 
    -E make_directory ${DOWNLOAD_PATH} )
endif()

if(NOT EXISTS "${DOWNLOAD_PATH}/.git")
  execute_process( COMMAND ${GIT_EXECUTABLE} 
    clone ${REPO_URL} ${DOWNLOAD_PATH} )    
else( )
  message(STATUS "Path already contains a GIT repository. Not cloning")
endif()


