

macro(CheckExternalDependencies projName errorVar)

  unset(EXT_DEP_ERR)

  foreach (projExtDep ${${projName}_external_dependencies})
    message(STATUS "Checking for ${projExtDep}")
    find_package(${projExtDep})
    string(TOUPPER ${projExtDep} projExtDepUpper)
    if ((${projExtDep}_FOUND) OR (${projExtDepUpper}_FOUND))
    else()
      message(STATUS "Error: external dependency " ${projExtDep} " not found")
      set(${errorVar} "1")
    endif()
  endforeach()

endmacro()


include(ExternalProject)

function(CreateTargets projName)

  if (NO_SRC_DOWNLOAD)
    message("Warning: no source download activated.")
    set(JAMBS_EP_DOWNLOAD_COMMAND DOWNLOAD_COMMAND
      "echo" "JAMBS: skipping source download rule for ${projName}")
    set(JAMBS_EP_UPDATE_COMMAND UPDATE_COMMAND
      "echo" "JAMBS: skipping source update rule for ${projName}")
  endif()


  set(JAMBS_DOWNLOAD_COMMAND "")

  if (${${projName}_skip_download})
    set(JAMBS_DOWNLOAD_COMMAND DOWNLOAD_COMMAND "echo" "-n")
    set(JAMBS_EP_DOWNLOAD_COMMAND #DOWNLOAD_COMMAND
      "echo" "JAMBS: skipping source download rule for ${projName}")
  else()

    if (${${projName}_repo_type} STREQUAL "git")
      set(JAMBS_DOWNLOAD_COMMAND DOWNLOAD_COMMAND "echo" "-n")
      set(JAMBS_EP_DOWNLOAD_COMMAND
        ${CMAKE_COMMAND}
        -DGIT_EXECUTABLE=${GIT_EXECUTABLE}
        -DREPO_URL=${${projName}_repo_url}
        -DDOWNLOAD_PATH=${CMAKE_CURRENT_SOURCE_DIR}/src/${projName}
        -P ${CMAKE_SOURCE_DIR}/CMake/DownloadCommand.cmake )
      #    message(${JAMBS_EP_DOWNLOAD_COMMAND})
    endif()

    if (${${projName}_repo_type} STREQUAL "tgz")
      if ( EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/src/${projName}/CMakeLists.txt )
        message(STATUS "src/${projName} directory exists. Download will be skipped")
        set(JAMBS_DOWNLOAD_COMMAND DOWNLOAD_COMMAND
          "echo" "src/${projName} directory exists. Skipping download")
      endif()
    endif()

  endif()

  if ( NOT $ENV{JAMBS_${projName}_repo_tag} STREQUAL "" )
    set( ${projName}_repo_tag $ENV{JAMBS_${projName}_repo_tag} )
    message(STATUS "Setting ${projName} tag to ${${projName}_repo_tag}")
  endif( )

  if (${${projName}_repo_type} STREQUAL "git")
    set(JAMBS_EP_REPO_DOWNLOAD_PROPERTIES
      GIT_REPOSITORY "${${projName}_repo_url}"
      GIT_TAG "${${projName}_repo_tag}" )
  endif()

  if (${${projName}_repo_type} STREQUAL "svn")
    set(JAMBS_EP_REPO_DOWNLOAD_PROPERTIES
      SVN_REPOSITORY "${${projName}_repo_url}" )
  endif()

  if (${${projName}_repo_type} STREQUAL "tgz")
    set(JAMBS_EP_REPO_DOWNLOAD_PROPERTIES
      URL "${${projName}_repo_url}" )
  endif()


  if (${projName}_no_install_rule)
    set(JAMBS_EP_INSTALL_COMMAND INSTALL_COMMAND
      "echo" "JAMBS: skipping install rule for ${projName}" )
  endif()

  if (${projName}_patch)
    set(JAMBS_EP_PATCH_COMMAND PATCH_COMMAND
      "${CMAKE_CURRENT_SOURCE_DIR}/CMake/patch.sh"
      "<"  ${CMAKE_CURRENT_SOURCE_DIR}/${${projName}_patch} )
  endif()

  #
  # Add ExternalProject with the properties defined by
  # its config file and/or global configurations
  #
  set_property(DIRECTORY PROPERTY EP_STEP_TARGETS
    configure build test install)

  ExternalProject_Add(
    "${projName}"
    # Prefix
    PREFIX "${CMAKE_CURRENT_BINARY_DIR}/${projName}"

    # Download rule properties
    ${JAMBS_DOWNLOAD_COMMAND}

    # Specific repo type properties
    ${JAMBS_EP_REPO_DOWNLOAD_PROPERTIES}

    # Update command
    ${JAMBS_EP_UPDATE_COMMAND}

    # Patch if needed
    ${JAMBS_EP_PATCH_COMMAND}

    # Source dir
    SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/src/${projName}"

    # CMake specific options for configuration rule
    CMAKE_GENERATOR "${CMAKE_GENERATOR}"
    CMAKE_GENERATOR_TOOLSET ${CMAKE_EXTRA_GENERATOR}
    CMAKE_ARGS
    "-DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}"
    "-DCMAKE_INSTALL_PREFIX:PATH=${CMAKE_CURRENT_BINARY_DIR}/install"
    "-DCMAKE_PREFIX_PATH:PATH=${CMAKE_CURRENT_BINARY_DIR}/install"
    "${${projName}_extra_config_params}"

    # Binary dir
    BINARY_DIR "${CMAKE_CURRENT_BINARY_DIR}/${projName}"

    # Install rule specific properties
    INSTALL_DIR "${CMAKE_CURRENT_BINARY_DIR}/install"
    ${JAMBS_EP_INSTALL_COMMAND}
    )


  ExternalProject_Add_StepTargets("${projName}" download-src)

  ExternalProject_Add_Step("${projName}" download-src
    COMMAND ${JAMBS_EP_DOWNLOAD_COMMAND}
    #    DEPENDEES steps...]    # Steps on which this step depends
    DEPENDERS update
    ALWAYS 1              # No stamp file, step always runs
    # [WORKING_DIRECTORY dir] # Working directory for command
    # [LOG 1]                 # Wrap step in script to log output
    )




  # Windows CMAKE seems to execute this rules always
  if (NOT MSVC)
    if (${${projName}_repo_type} STREQUAL "git")
      add_custom_target("${projName}-git-status"
        COMMAND "${GIT_EXECUTABLE}" "status"
        WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/src/${projName}"
        )

      add_dependencies(git-status "${projName}-git-status")

      add_custom_target("${projName}-src-update"
        COMMAND "${GIT_EXECUTABLE}" "pull"
        WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/src/${projName}"
        )

      add_dependencies(src-update "${projName}-src-update")

    endif()

    add_custom_target("${projName}-clean"
      COMMAND "make" "clean"
      WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/${projName}"
      )

    add_dependencies(clean-all "${projName}-clean")

  endif()


endfunction()

