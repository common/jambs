#
# Find every config directory begining with jambs-config
#
file(GLOB CONFIG_DIRS "${CMAKE_CURRENT_SOURCE_DIR}/jambs-config*")

#
# For each configuration targets are loaded and direcotry is added to
# the module path.
#
foreach(CONFIG_DIR ${CONFIG_DIRS})
  if(IS_DIRECTORY "${CONFIG_DIR}")
    message(STATUS "Processing config dir : " ${CONFIG_DIR})
    list(APPEND CMAKE_MODULE_PATH ${CONFIG_DIR})
    file(READ ${CONFIG_DIR}/targets.txt ${CONFIG_DIR}_TARGETS_LINES)

    string(REGEX REPLACE "\n" ";"  ${CONFIG_DIR}_TARGETS_LINES  "${${CONFIG_DIR}_TARGETS_LINES}")

    foreach (LINE ${${CONFIG_DIR}_TARGETS_LINES})
      if ("${LINE}" MATCHES "#.*")
        message(STATUS "skipping line " ${LINE})
      else()
        list(APPEND JAMBS_AUTO_TARGETS ${LINE})
      endif()
    endforeach()
  endif()
endforeach()
