



function(svn_external DIR REPO TAG)


# Load git external functions 
include(FindSubversion)


if(NOT Subversion_SVN_EXECUTABLE)
  message(FATAL_ERROR "svn not found")
endif()
  if (IS_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/${DIR}/.svn")

    execute_process(
      COMMAND "${Subversion_SVN_EXECUTABLE}" "update"  
      RESULT_VARIABLE result 
      ERROR_VARIABLE error
      WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/${DIR}" 
      )
    if(result)
      message(FATAL_ERROR "${DIR} svn checkout failed: ${error}\n")
    endif()
    
  else()
    
    execute_process(
      COMMAND "${Subversion_SVN_EXECUTABLE}" "checkout" "${REPO}" "${CMAKE_CURRENT_SOURCE_DIR}/${DIR}" 
      RESULT_VARIABLE result 
      ERROR_VARIABLE error
      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR} # Not necessary
      )    
    if(result)
      message(FATAL_ERROR "${DIR} svn checkout failed: ${error}\n")
    endif()

  endif()

endfunction()
