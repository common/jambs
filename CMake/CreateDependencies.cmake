


function(CreateDependencies projName)

  foreach (projDep ${${projName}_depends})
    if ( TARGET ${projDep} )
      add_dependencies(${projName} ${projDep})
    else( )
      message(STATUS "${projDep} target doesnt exists. Skipping it for project ${projName}")
    endif( )
  endforeach()

endfunction()
