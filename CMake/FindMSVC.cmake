
if ("${MSVC}") 
  find_program(
    MSBUILD msbuild.exe 
    "c:/Windows/Microsoft.NET/Framework/v4.0.30319" )

  find_program(
    DEVENV devenv.exe 
	"c:/Program\ Files\ (x86)/Microsoft\ Visual\ Studio\ 10.0/Common7/IDE/" 
    "c:/Program\ Files/Microsoft\ Visual\ Studio\ 10.0/Common7/IDE/" 
  )

  message(STATUS "MSBuild: " "${MSBUILD}")
  if ("${MSBUILD}" STREQUAL "MSBUILD-NOTFOUND")
    MESSAGE(FATAL_ERROR "MSBuild: not found")
  endif()
  
  message(STATUS "devenv: " "${DEVENV}")
  if ("${DEVENV}" STREQUAL "DEVENV-NOTFOUND")
    MESSAGE(FATAL_ERROR "devenv: not found")
  endif()
  
endif()
